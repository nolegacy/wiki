---
title: Distros
description: Linux distributions that offer alternatives to systemd.
---
### Non-linux
- [NetBSD](https://www.netbsd.org/)
    - NetBSD focuses on security and realiability
- [FreeBSD](https://www.freebsd.org/)
    - FreeBSD focuses on ease-of-use and compatability

### Non-GNU
-   [Alpine](https://www.alpinelinux.org/)
    - Alpine Linux is a security-oriented, lightweight Linux distribution based on musl libc and busybox.
-   [Chimera](https://chimera-linux.org/)
    - Chimera is a clean, modern and easy-to-use distro based on the linux kernel and bsd.

### Non-Glibc
-   [KISS](https://kisslinux.org/)
    - A Linux meta-distribution for the x86_64 architecture with a focus on simplicity, sustainability and user freedom. 

### GlibC optional
-   [Void](https://voidlinux.org/)
    - Void Linux is an independent operating system with a stable rolling release, runit as its init system, and support for both musl and GNU libc implementations.
-   [Gentoo](https://www.gentoo.org/)
    - a flexible Linux distro supporting Glibc and musl, gcc and LLVM, systemd and openRC. Highly customizable and source-based, offering unparalleled freedom for users.
-   [Funtoo](https://www.funtoo.org/Welcome)
    - Funtoo Linux is a community-driven meta-distribution. It prioritizes performance, offers easier installation, pre-built kernel, ready-to-use desktops, and easier USE.
    -   Funtoo does not plan on supporting systemd and actively avoids it [Link](https://www.funtoo.org/Mitigating_Systemd)
### Glibc
-   [Slackware](http://www.slackware.com/)
-   [Guix](https://guix.gnu.org/)
-   [Artix](https://artixlinux.org/)
    -   Supports OpenRC, runit and s6
-   [Devuan](https://devuan.org/)
-   [Obarun](https://web.obarun.org/)
    -   Supports s6, along with their own s6 wrapper, 66-tools, that provides a systemd-like experience
-   [Parabola](https://www.parabola.nu/)
-   [Hyperbola](https://www.hyperbola.info/)


Find other systemd-less distros on [DistroWatch](https://distrowatch.com/search.php?ostype=All&category=All&origin=All&basedon=All&notbasedon=None&desktop=All&architecture=All&package=All&rolling=All&isosize=All&netinstall=All&language=All&defaultinit=Not+systemd&status=Active#simple)
