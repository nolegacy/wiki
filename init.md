---
title: Init systems
---

### Alternatives
Good alternatives to systemd
-   [sysvinit](https://wiki.gentoo.org/wiki/Sysvinit) - the original init
-   [OpenRC](https://wiki.gentoo.org/wiki/OpenRC) - a dependency-based init system
    -   [openrc-init](https://wiki.gentoo.org/wiki/OpenRC/openrc-init) - a sysvinit replacement
-   [s6](https://www.skarnet.org/software/s6) - a daemontools-inspired process supervisor
    -   [s6-linux-init](https://skarnet.org/software/s6-linux-init/) - s6-based Linux init system
    -   [s6-rc](https://skarnet.org/software/s6-rc/) - service manager
    -   [s6-overlay](https://github.com/just-containers/s6-overlay) - integrates s6 with Docker images
    -   [66-tools](https://web.obarun.org/software/66-tools/) - a suite of s6 helpers made by the creators of the Obarun distribution
-   [runit](http://smarden.org/runit/) - a minimalistic daemontools-inspired process supervisor
-   [Upstart](https://wiki.ubuntu.com/Upstart) - an init replacement for sysvinit made by Canonical
-   [GNU Shepherd](https://www.gnu.org/software/shepherd/) - init system and service manager made by GNU, intended for [GNU/Hurd](https://www.gnu.org/software/hurd/) and [GNU/Guix](https://guix.gnu.org/index.html)
- [Finit](https://troglobit.com/projects/finit/) - Finit is an alternative to init system, originally reverse engineered from the EeePC fastinit
- [Hummingbird](https://github.com/Sweets/hummingbird) - hummingbird is an init system designed for speed. It can only start and stop the system.
- [cinit](https://www.nico.schottelius.org/software/cinit/) - cinit is a fast init system with dependency features and profile support.
- [initng](https://github.com/initng/initng) - Initng is a faster and modern replacement for sysvinit, doing as much as possible asynchronously
- [dinit](https://github.com/davmac314/dinit) - Dinit is a service supervisor that focuses on portability, clean design, robustness, and usability.

### The bad 
Why systemd sucks
- It does not adhere to the Unix philosophy and attempts to handle everything (for example, DNS, bootloader, and networking).

- Systemd was designed essentially on the presumption that nobody other than systemd would be using cgroups, and they even tried to lobby to make cgroups a private property of systemd in the kernel(Making the kernel **only** work with systemd) but that didn't work either. Systemd appropriates the cgroup tree, takes control of it, and completely messes with any other user of the cgroup tree. Systemd's use of cgroups for process tracking is a fundamentally broken concept; cgroups were never meant for this (making systemd by definition a "hack"), and it's a good way to screw up resource usage.

- Systemd has a strong requirement on glibc for no apparent reason, limiting it to Glibc-based linux systems.

- Systemd relies on DBus for IPC; however, as the term 'Desktop bus' says, DBus was never developed with this in mind, and it shows. DBus was not designed as a transport during early boot, but rather to support IPC within a single desktop session. This is why systemd wanted to promote kdbus so hard, as kdbus fixed several issues with DBus being used as IPC during early boot.

- Systemd's security and general code quality methods are subpar; many security flaws appear in systemd as a result of its insistence on stuffing a lot of code into pid1, adding new ~~bloat~~ features ontop of thousands of lines of bloated code.

- Systemd doesnt care about what maintainers want, by the creator of systemd [source](https://lists.freedesktop.org/archives/systemd-devel/2010-September/000391.html)

#### list
- [systemd-importd](https://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n236)
    - This is pure evil. Your pid 1 is now able to import complete system images over the network and show them to you as your running system. There is nothing that can go wrong.
- [X11 in systemd](http://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n783)
    - Of course graphics were missing in pid 1.
- [Calendar](http://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n1044)
    - As you see, your pid 1 should handle your calendars and cron jobs too.
- [pid 1 does DNS](http://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n1128)
    - This change adds another open door to your pid 1, adds caches, new APIs and of course it will never fail and break systemd on your initial boot.
- [Clean up directories](http://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n1306)
    - There is another monster in systemd, it handles tmp files. There are just some cases before it was introduced to have to clean up a directory in the file tree. Now there are hundreds. And easily another case can be added! Of course your init process does that.
- [Factory reset](http://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n1401)
    - Welcome to the Windows OEM world: Factory reset for Linux! Of course it is in your init process.
- [init does man](http://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n1465)
    - My init process is too big, it needs its own file hierarchy and an abstraction layer to find paths.
- [remote in pid 1](http://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n1500)
    - »Everything will end up having a remote API.« I wonder when systemd will understand MIME and e-mail.
- [New = better](http://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n1571)
    - The systemd development process is flawed by always assuming »new is best«. 
    - Network configuration should be in my init process.
- [sytemd-resolved](http://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n1578)
    - Every configuration file needs its own process and service.
    - Symlinks are a good way to solve all world problems.
- [hostnamed](http://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n1763)
    - There really should be a process running which exposes the content of a file. Complexity is without cost.
- [screen brightness](http://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n1763)
    - There really should be a process running which exposes the content of a file. Complexity is without cost.
- [logind should wait](http://cgit.freedesktop.org/systemd/systemd/tree/NEWS?id=2d1ca11270e66777c90a449096203afebc37ec9c#n2089)
    - systemd was introduced to decrease the boot up time. Now that they do not understand all implications and dependencies, let us add some artifical time we found out might work for the developers laptops. More on this small world hypothesis of the systemd developers below.
- [Systemd does your dns](https://lists.dns-oarc.net/pipermail/dns-operations/2016-June/014964.html)
- [Systemd hates when you change your system](https://bugzilla.redhat.com/show_bug.cgi?id=817186)
- [Systemd locks down /etc and makes it read-only](http://bugzilla.redhat.com/show_bug.cgi?id=1350450)
- [Systemd does unix NICE](https://github.com/systemd/systemd/blob/76153ad45f09b6ae45464f2e03d3afefbb4b2afe/NEWS#L425)
- [systemd-nspawn can patch at will any kind of file in a container](https://github.com/systemd/systemd/blob/76153ad45f09b6ae45464f2e03d3afefbb4b2afe/NEWS#L391)
    - Paired with transient units and user escalation performable remotely, this can mean that if you house VPS instances somewhere, your hosting provider has means and tools to spy, modify, delete any kind of content you store there. Encrypt everything, read your TOS.
- [systemd-logind does sighup and nohup](https://github.com/systemd/systemd/blob/76153ad45f09b6ae45464f2e03d3afefbb4b2afe/NEWS#L274)
    - Logout is equivalent to shutting off the machine, so you will NOT have any running program after logout, unless you inform your init system.
- [systemd does socat/netcat](https://github.com/systemd/systemd/blob/76153ad45f09b6ae45464f2e03d3afefbb4b2afe/NEWS#L546)
- [Transient units](https://github.com/systemd/systemd/blob/76153ad45f09b6ae45464f2e03d3afefbb4b2afe/NEWS#L708)
    - Temporary services, because we love to reinvent procps, forking, nohup and lsof.
- [systemd-journald can do log-rotate](https://github.com/systemd/systemd/blob/76153ad45f09b6ae45464f2e03d3afefbb4b2afe/NEWS#L1073)
    - Being journal files binaries written with easily corruptable transactions, does this feature make the log unreadable at times?
- [systemd replaces sudo and su](https://github.com/systemd/systemd/blob/76153ad45f09b6ae45464f2e03d3afefbb4b2afe/NEWS#L1233i)
    - Please note the command name, machinectl and its [features at the manpage](https://www.freedesktop.org/software/systemd/man/machinectl.html). In exchange for a program which contains sudo, su and kill (and does some functions which historically ssh/telnet did), bare metal users have a tons of bloat and a lot of things to disable, if even possible, useful only to people which deal with virtual machines.
- [Systemd is now a bootloader](https://github.com/systemd/systemd/blob/76153ad45f09b6ae45464f2e03d3afefbb4b2afe/NEWS#L1306)
    - Should systemd's PID be changed from 1 to a negative, or imaginary, number? It now exists before the kernel itself, during a bootup.
- [Systemd breaks the mouse(again)](https://github.com/systemd/systemd/issues/8579)
    - I dont even know how an init system breaks the mouse
- [systemd will be able to kill a process if it needs/wants more RAM](https://github.com/systemd/systemd/pull/15206)
- [Don't panic, but Linux's Systemd can be pwned via an evil DNS query](https://www.theregister.co.uk/2017/06/29/systemd_pwned_by_dns_query/)
- [Systemd Could Fallback to Google DNS?](https://isc.sans.edu/forums/diary/Systemd+Could+Fallback+to+Google+DNS/22516/)
- [DNS search domain not removed from resolv.conf on disconnect](https://bugs.launchpad.net/ubuntu/+source/network-manager/+bug/1713457)
- [systemd does not respect system wide resource limits](https://fredrikaverpil.github.io/2016/04/27/systemd-and-resource-limits/)
- [Systemd causes outage on Azure](https://bugs.launchpad.net/ubuntu/+source/systemd/+bug/1988119)
    - Widespread outage was caused on Azure, when systemd 237-3ubuntu10.54 was published to the bionic-security pocket (instances could no longer resolve DNS queries, breaking networking)
- [Systemd Renames Network Interfaces](https://lwn.net/Articles/758128/)
    - Because breaking the name of my network adapter is exactly what I was asking for.
- ["lamest vendor"](https://www.theregister.com/2017/07/28/black_hat_pwnie_awards/)
    - The annual pwnie awards awarded systemd its "Lamest Vendor" prize in 2017, due to its handling of bugs 5998, 6225, 6214, 5144, and 6237.
- [a given DNS response will cause systemd to hang entirely](https://cve.mitre.org/cgi-bin/cvename.cgi?name=2017-15908)
    - Wow - nobody ever warned that there was a massive danger inherent in putting so much arbitrary code into PID 1, did they?
- [list-dependencies]()
    - `systemctl list-dependencies --after gdm.service` However, this lists all things that contain an "After: gdm-service" in their definition, not the things that are after gdm.service in the dependencies tree.


### Resources
- [suckless](https://suckless.org/sucks/systemd/) - "There is a menace which is spreading like a disease throughout the Linux world, it is called systemd."
- [darkedgy](https://blog.darknedgy.net/technology/2020/05/02/0/index.html) - "I am not sure I am such a big fan of reimplementing NetworkManager…"
- [nosystemd](https://nosystemd.org/) - "If this is the solution, I want my problem back." 
- [ihatesystemd](https://ihatesystemd.com) - "Some things about systemd aren't necessarily bad, they're just damned Ugly."
- [Systemd: broken by design](https://ewontfix.com/14/) - "systemd is broken by design"
- [Systemd: Harbinger of the Linux apocalypse](https://www.infoworld.com/article/2608798/systemd--harbinger-of-the-linux-apocalypse.html) - "It might not be the end of the world, but the design of systemd and the attitudes of its developers have been counterproductive"
