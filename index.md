## Welcome
Welcome to this wiki, where you'll find a wide range of based alternatives to legacy software. 
Use the sidebar to navigate. Join our community on Discord to contribute to this wiki and access helpful bots, resources, and support. 
We look forward to having you on board!

## Why?
We created this page with the intention of educating people about the benefits of embracing modern solutions. Here's why it matters:

By moving away from legacy software, we enable the Linux desktop to progress and evolve. If we continue relying on systemd, we hinder the development of a superior init system. Similarly, persisting with GlibC results in a perpetually bloated C library. While compatibility is important, it should not always be the top priority. Instead, we should focus on supporting newer hardware and software to drive innovation and efficiency.
