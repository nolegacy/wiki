This page includes modern replacement for legacy packaging formats such as rpm

- [Flatpak](https://flatpak.org/)
  - Flatpak is a sandboxed containarized packaging format that works on any distro(even non-gnu ones)
- [LingLong](https://linglong.dev/)
  - Linglong is the only sandboxed packaging format besides flatpak its made to fix compatability issues
