- [Musl](https://musl.libc.org/) - A lightweight, fast, simple, and standards-compliant C library implementation. It aims to be correct in terms of standards-conformance and safety.

- [BSD libc](https://cgit.freebsd.org/src/tree/lib/libc) - A family of C standard library implementations used in various BSD operating systems, such as FreeBSD, NetBSD, and OpenBSD.

- [PDCLib](https://github.com/DevSolar/pdclib) - A public domain C library implementation that aims to be minimal, portable, and conformant to C standards.

